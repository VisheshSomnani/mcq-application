<?php
class Auth{
    protected $database;
    
    protected $table = "users";
    protected $authSession = "users";
    
    public function __construct(Database $database){
        $this->database = $database;
    }
    
    public function signIn($data){
        if($data["usertype"] === "student"){
            $query = "SELECT * FROM students WHERE enrollment_number= :enrollment_number OR seat_number= :seat_number";
            $placeholders = [
                "enrollment_number" => $data['username'],
                "seat_number" => $data['username']
            ];
            $student = $this->database->prepare($query, $placeholders);
            if($this->database->count() == 1){
                $this->database->prepare("SELECT * FROM users WHERE usertype='student' AND password=:password", ["password" => $data["password"]]);
                if($this->database->count()==1){
                    $this->setSession("student", $student[0]["id"]);
                    $this->authSession = "student";
                    $this->setTestData();
                    return true;
                }
            }
        }else if($data["usertype"] === "admin"){
            $user=$this->database->prepare("SELECT * FROM users WHERE usertype=:usertype AND password=:password", ["usertype"=>$data["username"], "password"=>$data["password"]]);
            if($this->database->count()==1){
                $this->setSession("admin", $user[0]["id"]);
                $this->authSession = "admin";
                return true;
            }
        }
        return false;
    }
    
    public function setSession($sessionName, $value){
        $_SESSION[$sessionName] = $value;
    }
    
    public function getSession($sessionName){
        if(isset($_SESSION[$sessionName])){
            return $_SESSION[$sessionName];
        }
        return -1;
    }
    
    public function check(){
        return isset($_SESSION[$this->authSession]);
    }

    public function unsetSession($sessionName){
        unset($_SESSION[$sessionName]);
    }
    
    public function user($table){
        if(!$this->check()){
            return false;
        }
        $user = $this->database->table($table)->where("id", "=", $_SESSION[$this->authSession])->first();
        return $user;   
    }
    
    public function setTestData(){
        $date = date("Y-m-d", time());
        $test = $this->database->rawQueryExecutor("SELECT * FROM tests where conduct_on='$date'");
        // die(var_dump($test));
        if($this->database->count()==1){
            $test_id = $test[0]['id'];
//            die(var_dump($test));
            $test_details = $this->database->rawQueryExecutor("SELECT * FROM test_details where test_id=$test_id");
            $test_data = ["test" => $test[0], "test_details" => $test_details];
            $this->setSession("test_data", $test_data);
        }
    }
}
?>