<?php
require_once("../includes/init.php");
if(isset($_POST["data"])){
    $data = (array)json_decode($_POST["data"]);
    $dataToPass = array();
    if(strpos($data["usertype"], "/student") > 0){
        $dataToPass = [
            "usertype" => "student",
            "username" => substr($data["usertype"], 0, strpos($data["usertype"], "/student")),
            "password" => $data["password"]
        ];
    }else{
        $dataToPass = [
            "usertype" => "admin",
            "username" => $data["usertype"],
            "password" => $data["password"]
        ];
    }
    $output = ["result" => false];
    if($auth->signIn($dataToPass)){
        $output["result"] = true;
    }
    $JSONData = json_encode($output);
    echo $JSONData;
}
?>