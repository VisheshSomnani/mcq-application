<?php
require_once("../includes/init.php");
if(isset($_POST["type"])){
    $test_details =$auth->getSession("test_data")["test_details"];
//    print_r($test_details);
    $queries_for_question = array();
    $queries_for_correct_answer = array();
    foreach($test_details as $row){
        array_push($queries_for_question, ("SELECT * FROM questions where subject_id = " . $row['subject_id'] . " AND chapter_number = " . $row['chapter_number'] . " AND type = " . $_POST["type"]));
        array_push($queries_for_correct_answer, ("SELECT correct_answer.* FROM correct_answer, questions where questions.subject_id = " . $row['subject_id'] . " AND questions.chapter_number = " . $row['chapter_number'] . " AND type = " . $_POST["type"] . " AND correct_answer.question_id = questions.id"));
    }
    $query_for_options = "SELECT * FROM options WHERE question_id = ";
//    print_r($queries_for_correct_answer);
    $questions = array();
    $options = array();
    $correct_answer = array();
    foreach($queries_for_question as $query){
        array_push($questions, $database->rawQueryExecutor($query));
    }
    foreach($queries_for_correct_answer as $query){
        array_push($correct_answer, $database->rawQueryExecutor($query));
    }
    for($i=0; $i<count($questions); $i++){
        $options_for_chapter = array();
        for($j=0; $j<count($questions[$i]); $j++){
            $options_for_a_question = $database->rawQueryExecutor($query_for_options . $questions[$i][$j]["id"]);
            array_push($options_for_chapter, $options_for_a_question);
        }
        array_push($options, $options_for_chapter);
    }
    $data = ["questions" => $questions, "options" => $options, "correct_answer" => $correct_answer];
    echo json_encode($data);
}
?>