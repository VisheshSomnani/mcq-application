<?php
require_once("../includes/init.php");
if(isset($_POST["data"])){
    $data = (array)json_decode($_POST["data"]);
    $data = $database->prepare($data["query"], (array)$data["placeholders"]);
    $JSONData = json_encode($data);
    echo $JSONData;
}
?>