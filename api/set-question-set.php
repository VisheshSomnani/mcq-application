<?php
require_once("../includes/init.php");
if(isset($_POST["set"])){
    $test_id =$auth->getSession("test_data")["test"]["id"];
    $student_id = $auth->getSession("student");
    $data = (array)json_decode($_POST["set"]);
    $query = "INSERT INTO student_test_data (test_id, student_id, alloted_question_id, selected_option_id) VALUES(:test_id, :student_id, :alloted_question_id, :selected_option_id)";
    for($i=0; $i<count($data["questions"]); $i++){
        for($j=0; $j<count($data["questions"][$i]); $j++){
            $placeholders = [
                "test_id" => $test_id,
                "student_id" => $student_id,
                "alloted_question_id" => $data["questions"][$i][$j],
                "selected_option_id" => $data["option"][$i][$j]
            ];
            $result = $database->prepare($query, $placeholders);
        }
    }
    $query = "INSERT INTO marks_obtained (test_id, student_id, obtained_marks) VALUES(:test_id, :student_id, :obtained_marks)";
    $placeholders = [
        "test_id" => $test_id,
        "student_id" => $student_id,
        "obtained_marks" => $data["marksObtained"]
    ];
    $result = $database->prepare($query, $placeholders);    
    $auth->unsetSession("student");
    $auth->unsetSession("test_data");
    echo json_encode($result);
}
?>