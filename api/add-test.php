<?php
require_once("../includes/init.php");
if(isset($_POST["data"])){
    $test = (array)json_decode($_POST["data"])->test;
    $test_details = (array)json_decode($_POST["data"])->test_details;
//    die(var_dump($test_details));
    $query = "INSERT INTO tests(name, conduct_on, duration, marks) VALUES(:name, :conduct_on, :duration, :marks)";
    $result = $database->prepare($query, $test);
    $test_id = $database->lastInsertId();
//    die(var_dump($test_id));
    $query = "INSERT INTO test_details(test_id, subject_id, chapter_number, marks) VALUES($test_id, :subject_id, :chapter_number, :marks)";
    foreach($test_details as $test_detail){
        $result = $database->prepare($query, (array)$test_detail);
    }
    $auth->unsetSession("admin");
    echo json_encode($result);
}
?>