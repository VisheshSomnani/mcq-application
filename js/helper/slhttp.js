class slHTTP{
    async get(url){
        const response = await fetch(url);
        const data = await response.text();
        return data;
    }

    async post(url, data){
        const response = await fetch(url, {
            method: 'POST',
            body: data
        });
        
        const resData = await response.text();
        return resData;
    }
    async put(url, data){
        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        const resData = await response.json();
        return resData;
    }

    async delete(url){
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json'
            }
        });

        const resData = await 'Resouece Deleted...';
        return resData;
    }
}