const http = new slHTTP();
const fd = new FormData();
const baseURL = "http://localhost:8888/";
const EASY = 1;
const MEDIUM = 2;
const HARD = 3;

const hours = document.getElementById("hours");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds");
const save = document.getElementById("save");
const next = document.getElementById("next");
const previous = document.getElementById("previous");
const questionCount = document.getElementById("question-count");
const question = document.getElementById("question");
const questionNumber = document.getElementById("question-number");
const optionButtons = document.getElementsByClassName("option_btn");
var optionLabels = [];
optionLabels.push(document.getElementById("option1_label"));
optionLabels.push(document.getElementById("option2_label"));
optionLabels.push(document.getElementById("option3_label"));
optionLabels.push(document.getElementById("option4_label"));

var testData;

var answeredId = -1;
var questions = [];
var options = [];
var correctAnswers = [];
var selectedQuestions = [];
var selectedOptions = [];
var row = 0;
var col = 0;
var totalQuestions = 0;
var currentQuestionNumber = 0;
var countDown;
var marksObtained=0;


document.addEventListener('DOMContentLoaded', bindListeners);

function bindListeners(){
    save.addEventListener("click", saveAnswer);
    next.addEventListener("click", nextQuestion);
    previous.addEventListener("click", previousQuestion);
    optionButtons[0].addEventListener("click", setAnswer);
    optionButtons[1].addEventListener("click", setAnswer);
    optionButtons[2].addEventListener("click", setAnswer);
    optionButtons[3].addEventListener("click", setAnswer);
    xhrCall("get", "test_data", "api/getter.php", setTestData);
}

function setAnswer(e){
    answeredId = e.target.dataset.id;
}

function signal(data){
    //console.log(data);
}
function nextQuestion(){
    if(next.textContent.indexOf("Next") > -1){
        currentQuestionNumber++;
        if(col == selectedQuestions[row].length-1){
            col = 0;
            row++;
        }else{
            col++;
        }
        setQuestion();
    }else{
        stopTest();
    }
}

function setQuestionCount(){
    questionCount.textContent = "(" + currentQuestionNumber + " of " + totalQuestions + ")";
}

function previousQuestion(){
    currentQuestionNumber--;
    if(col == 0){
        row--;
        col = selectedQuestions[row].length-1;
    }else{
        col--;
    }
    setQuestion();
}

function saveAnswer(){
    selectedOptions[row][col] = answeredId;
}

function setTestData(data){
    testData = JSON.parse(data);
    xhrCall("type", JSON.stringify(EASY), "api/get-question-set.php", setQuestionSet)
    setTimer();
}

function setQuestionSet(data){
    let questionSet = JSON.parse(data);
    questions = questionSet.questions;
    options = questionSet.options;
    correctAnswers = questionSet.correct_answer;
    //console.log(questions[0]);
    //console.log(correctAnswers[0]);
    selectQuestions();
    startCountDown();
}

function selectQuestions(){
    let i = 0;
    testData["test_details"].forEach(row => {
        selectedQuestionForAChapter = [];
        totalMarks = row["marks"];
        while(totalMarks > 0){
            random = (1 + Math.floor(Math.random() * questions[i].length-1));
            while(!selectedQuestionForAChapter.includes(random)){
                selectedQuestionForAChapter.push(random);
                totalMarks -= questions[i][random]["marks"];
                if(totalMarks < 0){
                    selectedQuestionForAChapter.pop();
                    totalMarks += questions[i][random]["marks"];
                }
            }
        }
        selectedQuestions.push(selectedQuestionForAChapter);
        i++;
    });
    for(let i=0; i<selectedQuestions.length; i++){
        let temp = [];
        for(let j=0; j<selectedQuestions[i].length; j++){
            temp.push(-1);
            totalQuestions++;
        }
        selectedOptions.push(temp);
    }
    //console.log(selectedQuestions);
    currentQuestionNumber = 1;
    setQuestion();
}

function setQuestion(){
    if(currentQuestionNumber === 1){
        previous.disabled = true;
    }else{
        previous.disabled = false;
    }
    
    if(currentQuestionNumber === totalQuestions){
        next.innerHTML = "<i class='fa fa-close mr-2'></i> End Test";
        next.classList.add("danger");
    }else{
        next.innerHTML = "Next <i class='fa fa-chevron-right ml-2'></i>";
        next.classList.remove("danger");
    }
    
    let index = selectedQuestions[row][col];
    question.textContent = questions[row][index]["question"];
    let optionsOfQuestion = options[row][index];
    if(optionsOfQuestion.length > 4){
        
    }else if(optionsOfQuestion.length <= 4){
        let i=0;
        for(i=0; i<optionsOfQuestion.length; i++){
            optionButtons[i].disabled = false;
            optionLabels[i].textContent = optionsOfQuestion[i]["answer"];
            optionButtons[i].dataset.id = optionsOfQuestion[i]["id"];
        }
        if(i < 3){
            for(i=i; i<4; i++){
                optionLabels[i].textContent = "";
                optionButtons[i].disabled = true;
            }
        }
    }
    
    if(selectedOptions[row][col] > 0){
        let selected = selectedOptions[row][col];
        for(i=0; i<optionsOfQuestion.length; i++){
            if(optionButtons[i].dataset.id == selected){
                optionButtons[i].checked = true;
                break;
            }
        }
    }else{
        for(i=0; i<optionsOfQuestion.length; i++){
            optionButtons[i].checked = false;
        }
    }
    setQuestionCount();
}

function setTimer(){
    const timer = testData["test"]["duration"].split(":");
    hours.innerHTML = timer[0];
    minutes.innerHTML = timer[1];
    seconds.innerHTML = timer[2];
}

function xhrCall(name, toSend, sendOn, callback){
    fd.append(name, toSend);
    http.post(sendOn, fd)
        .then(output => callback(output))
        .catch(err => console.log(err));
}

function startCountDown(){
    countdown = setInterval(function(){
        let secs = parseInt(seconds.innerHTML);
        let mins = parseInt(minutes.innerHTML);
        let hrs = parseInt(hours.innerHTML);
        if(secs === 0){
            if(mins === 0){
                hrs--;
                hours.innerHTML = hrs.toString().length == 1 ? ("0" + hrs) : hrs.toString();
                mins = 59;
            }else{
                mins--;
            }
            minutes.innerHTML = mins.toString().length == 1 ? ("0" + mins) : mins.toString();
            secs = 59;
        }else{
            secs--;
        }
        seconds.innerHTML = secs.toString().length == 1 ? ("0" + secs) : secs.toString();
        if(secs === 0 && hrs === 0 && mins === 0){
            stopCountDown();
            stopTest();
        }
    }, 1000);
}

function stopTest(){
    insertQuestionSet();
    document.location.href = baseURL + "login.html";
}

function insertQuestionSet(){
    let selectedQuestionsId = [];
    for(let i=0; i<selectedQuestions.length; i++){
        let temp = [];
        for(let j=0; j<selectedQuestions[i].length; j++){
            temp.push(questions[i][selectedQuestions[i][j]]["id"]);
            if(selectedOptions[i][j] === correctAnswers[i][selectedQuestions[i][j]]["option_id"]){
                marksObtained += parseInt(questions[i][selectedQuestions[i][j]]["marks"]);
            }
        }
        selectedQuestionsId.push(temp);
    }
    //console.log(JSON.stringify({questions: selectedQuestionsId, option: selectedOptions, marksObtained: marksObtained}));
    xhrCall("set", JSON.stringify({questions: selectedQuestionsId, option: selectedOptions, marksObtained: marksObtained}), "api/set-question-set.php", signal);
}

function stopCountDown(){
    clearInterval(countdown);
}