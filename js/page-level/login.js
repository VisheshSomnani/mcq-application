const http = new slHTTP();
const fd = new FormData();
const txtUsername = document.getElementById("username");
const txtPassword = document.getElementById("password");
const txtCapchaAnswer = document.getElementById("capcha_answer");
const txtCapchaQuestion = document.getElementById("capcha_question");
const errorMessage = document.getElementById("error-message");
const btnLogin = document.getElementById("btn_login");
const btnClose = document.querySelector(".close");
const errorBox = document.querySelector(".alert");
var capchaCorrectAnswer = 0;
var usertype = "admin";
const baseURL = "http://localhost:8888/";

document.addEventListener('DOMContentLoaded', bindListeners);

function bindListeners(){
    let no1 = (1 + Math.round(Math.random() * 9));
    let no2 = (1 + Math.round(Math.random() * 9));
    capchaCorrectAnswer = no1 + no2;
    txtCapchaQuestion.value = `${no1} + ${no2}`;
    btnLogin.addEventListener("click", authenticateUser);
    btnClose.addEventListener("click", closeErrorBox);
    console.log(errorMessage);
}

function closeErrorBox(e){
    e.preventDefault();
    errorBox.classList.add("hide-alert");
}

function authenticateUser(e){
    e.preventDefault();
    const userCapchaAnswer = parseInt(txtCapchaAnswer.value);
    if(!(userCapchaAnswer === capchaCorrectAnswer)){
        errorBox.classList.remove("hide-alert");
        errorMessage.textContent = "Incorrect Capcha! Enter again!";
        console.log(errorMessage);
    }else{
        let username = txtUsername.value;
        const regex = /^[0-9]/;
        if(regex.test(username)){
            username += "/student";
            usertype = "student";
        }
        const password = txtPassword.value;
        let data = {
            usertype: username,
            password: password
        };
        data = JSON.stringify(data);
        fd.append("data", data);
        http.post("api/login.php", fd)
            .then(output => checkUserType(output))
            .catch(err => console.log(err));
    }
}

function checkUserType(data){
    console.log(data);
    let answer = JSON.parse(data);
    if(answer["result"] === true){
        if(usertype === "student"){
            document.location.href = baseURL + "instruction-page.html";
        }else if(usertype === "admin"){
            document.location.href = baseURL + "add-test.html";
        } 
    }else{
        errorBox.classList.remove("hide-alert");
        errorMessage.textContent = "Incorrect username or password!";
        console.log(errorMessage);
    }
}