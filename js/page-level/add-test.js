const http = new slHTTP();
const fd = new FormData();
const baseURL = "http://localhost:8888/";

const addChapterBtn = document.getElementById("add_chapter_btn");
const testName = document.getElementById("name");
const testMarks = document.getElementById("marks");
const testConductOn = document.getElementById("conduct_on");
const testDuration = document.getElementById("duration");
const testSubjectId = document.getElementById("subject_id");
const addBtn = document.getElementById("add");
const chaptersWrapper = document.querySelector(".chapters_wrapper");

var selectedSubjectId = -1;
var chaptersStr = "";

bindListeners();
function bindListeners(){
    addChapterBtn.addEventListener("click", addChapter);
    addBtn.addEventListener("click", add);
    testSubjectId.addEventListener("change", emptyChaptersWrapper);
    xhrCall("query", "SELECT * FROM subjects", "api/query.php", fillSubjects);
}

function emptyChaptersWrapper(e){
    chaptersWrapper.innerHTML = "";
    selectedSubjectId = e.target.value;
    xhrCall("query", "SELECT * FROM chapters WHERE subject_id = " + selectedSubjectId, "api/query.php", updateChaptersStr);
}

function updateChaptersStr(data){
    console.log(data);
    chapterNumber = JSON.parse(data)[0];
    let output = "";
    for(let i=1; i<=chapterNumber.total_chapters; i++){
        output += `<option value="${i}">${i}</option>`;
    }
    chaptersStr = output;
}

function fillSubjects(data){
    subjects = JSON.parse(data);
    selectedSubjectId = subjects[0].id;
    xhrCall("query", "SELECT * FROM chapters WHERE subject_id = " + selectedSubjectId, "api/query.php", updateChaptersStr);
    subjects.forEach(subject => {
        let temp = document.createElement("OPTION");
        temp.value = subject.id;
        temp.textContent = subject.name;
        testSubjectId.appendChild(temp);
    });
}

function addChapter(e){
    e.preventDefault();
    var div = document.createElement("DIV");
    div.innerHTML = `
<div class="col-md-6">
    <div class="form-group">
        <label>Select Chapters</label>
        <select class="form-control">
` + chaptersStr + `
        </select>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label>Chapter's Marks</label>
        <input type="text" class="form-control" placeholder="Enter chapter's marks">
    </div>
</div>
`;
    div.className = "row";
    chaptersWrapper.appendChild(div);
}

function add(e){
    e.preventDefault();
    let name = testName.value;
    let marks = testMarks.value;
    let conductOn = testConductOn.value;
    let duration = testDuration.value;
    let chapters = [];
    let chapterMarks = [];
    for(let i=0; i<chaptersWrapper.childElementCount; i++){
        chapters.push(chaptersWrapper.children[i].children[0].children[0].children[1].value);
        chapterMarks.push(parseInt(chaptersWrapper.children[i].children[1].children[0].children[1].value))
    }
    let testDetails = [];
    for(let i=0; i<chapters.length; i++){
        placeholders = {
            subject_id: selectedSubjectId,
            chapter_number: chapters[i],
            marks: chapterMarks[i]
        };
        testDetails.push(placeholders);
    }
    let data = {
        test: {
            name,
            marks,
            conduct_on: conductOn,
            duration
        },
        test_details: testDetails
    };
    xhrCall("data", JSON.stringify(data), "api/add-test.php", signal);
}

function signal(data){
    console.log(data);
    document.location.href = baseURL + "login.html";
}

function xhrCall(name, toSend, sendOn, callback){
    fd.append(name, toSend);
    http.post(sendOn, fd)
        .then(output => callback(output))
        .catch(err => console.log(err));
}
