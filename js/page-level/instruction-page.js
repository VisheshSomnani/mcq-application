const http = new slHTTP();
const agreementChkbox = document.getElementById("agreement");
const startExamBtn = document.getElementById("start_exam");
const hours = document.getElementById("hours");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds");
const baseURL = "http://localhost:8888/";

document.addEventListener('DOMContentLoaded', bindListeners);

function bindListeners(){
    startExamBtn.disabled = true;
    startExamBtn.addEventListener("click", startExam);
    agreementChkbox.addEventListener("click", enableStartExam);
    http.get("api/getter.php")
        .then(output => setTimer(output))
        .catch(err => console.log(err));
}


function setTimer(data){
    const timer = JSON.parse(data)["test"]["duration"].split(":");
    console.log(timer);
    hours.innerHTML = timer[0];
    minutes.innerHTML = timer[1];
    seconds.innerHTML = timer[2];
}

function enableStartExam(){
    if(agreementChkbox.checked === true){
        startExamBtn.disabled = false;
    }else{
        startExamBtn.disabled = true;
    }
}

function startExam(){
    document.location.href = baseURL + "test.html";
}