-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2020 at 02:45 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcq_application`
--

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `total_chapters` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `subject_id`, `total_chapters`) VALUES
(1, 1, 6),
(2, 2, 6),
(3, 3, 6),
(4, 4, 6),
(5, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `correct_answer`
--

CREATE TABLE `correct_answer` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `correct_answer`
--

INSERT INTO `correct_answer` (`id`, `question_id`, `option_id`) VALUES
(1, 1, 2),
(2, 2, 7),
(3, 3, 9),
(4, 4, 14),
(5, 5, 17),
(6, 6, 22),
(7, 7, 23),
(8, 8, 28),
(9, 9, 33),
(10, 10, 37),
(11, 11, 39),
(12, 12, 45),
(13, 13, 47),
(14, 14, 53),
(15, 15, 57),
(16, 16, 59),
(17, 17, 66),
(18, 18, 68),
(19, 19, 72),
(20, 20, 76),
(21, 21, 82),
(22, 22, 83),
(23, 23, 85),
(24, 24, 91),
(25, 25, 96),
(26, 26, 99),
(27, 27, 103),
(28, 28, 105),
(29, 29, 112),
(30, 30, 115),
(31, 31, 120),
(32, 32, 121),
(33, 33, 125),
(34, 34, 131),
(35, 35, 133),
(36, 36, 138),
(37, 37, 141),
(38, 38, 145),
(39, 39, 151),
(40, 40, 155),
(41, 41, 157),
(42, 42, 161),
(43, 43, 165),
(44, 44, 169),
(45, 45, 176),
(46, 46, 177),
(47, 47, 181),
(48, 48, 187),
(49, 49, 190),
(50, 50, 196),
(51, 51, 197),
(52, 52, 201),
(53, 53, 207),
(54, 54, 212),
(55, 55, 214),
(56, 56, 220),
(57, 57, 222),
(58, 58, 667),
(59, 59, 225),
(60, 60, 230),
(61, 61, 233),
(62, 62, 237),
(63, 63, 241),
(64, 64, 247),
(65, 65, 249),
(66, 66, 254),
(67, 67, 259),
(68, 68, 261),
(69, 69, 266),
(70, 70, 270),
(71, 71, 275),
(72, 72, 277),
(73, 73, 284),
(74, 74, 286),
(75, 75, 289),
(76, 76, 293),
(77, 77, 300),
(78, 78, 301),
(79, 79, 307),
(80, 80, 312),
(81, 81, 313),
(82, 82, 317),
(83, 83, 321),
(84, 84, 327),
(85, 85, 331),
(86, 86, 335),
(87, 87, 338),
(88, 88, 343),
(89, 89, 345),
(90, 90, 350),
(91, 91, 353),
(92, 92, 358),
(93, 93, 361),
(94, 94, 367),
(95, 96, 372),
(96, 97, 378),
(97, 98, 380),
(98, 99, 385),
(99, 100, 389),
(100, 101, 393),
(101, 102, 395),
(102, 103, 399),
(103, 104, 401),
(104, 105, 407),
(105, 106, 409),
(106, 107, 416),
(107, 108, 420),
(108, 109, 423),
(109, 110, 426),
(110, 111, 431),
(111, 112, 433),
(112, 113, 439),
(113, 114, 441),
(114, 115, 445),
(115, 116, 449),
(116, 117, 453),
(117, 118, 459),
(118, 120, 467),
(119, 121, 471),
(120, 122, 474),
(121, 123, 478),
(122, 124, 479),
(123, 125, 485),
(124, 126, 487),
(125, 127, 494),
(126, 95, 373),
(127, 119, 461),
(128, 128, 497),
(129, 129, 501),
(130, 130, 506),
(131, 131, 507),
(132, 132, 511),
(133, 133, 516),
(134, 134, 520),
(135, 135, 525),
(136, 136, 529),
(137, 137, 534),
(138, 138, 537),
(139, 139, 540),
(140, 140, 545),
(141, 141, 550),
(142, 142, 551),
(143, 143, 558),
(144, 144, 561),
(145, 145, 565),
(146, 146, 568),
(147, 147, 571),
(148, 148, 577),
(149, 149, 580),
(150, 150, 583),
(151, 151, 587),
(152, 152, 591),
(153, 153, 595),
(154, 154, 597),
(155, 155, 601),
(156, 156, 607),
(157, 157, 610),
(158, 158, 614),
(159, 159, 618),
(160, 160, 623),
(161, 161, 625),
(162, 162, 630),
(163, 163, 633),
(164, 164, 637),
(165, 165, 641),
(166, 166, 647),
(167, 167, 649),
(168, 168, 653),
(169, 169, 660),
(170, 170, 661);

-- --------------------------------------------------------

--
-- Table structure for table `marks_obtained`
--

CREATE TABLE `marks_obtained` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `test_id` tinyint(3) NOT NULL,
  `obtained_marks` smallint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marks_obtained`
--

INSERT INTO `marks_obtained` (`id`, `student_id`, `test_id`, `obtained_marks`) VALUES
(9, 1, 1, 3),
(10, 2, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `question_id`, `answer`) VALUES
(1, 1, 'All Window To0lkit'),
(2, 1, 'Abstract Window Toolkit'),
(3, 1, 'All windows Tools'),
(4, 1, 'All Writing Tools'),
(5, 2, '1'),
(6, 2, '2'),
(7, 2, '3'),
(8, 2, '4'),
(9, 3, 'Model view controller'),
(10, 3, 'Model viewer controller'),
(11, 3, 'Modeless view controller'),
(12, 3, 'Modeless viewer controller'),
(13, 4, 'Platform dependent'),
(14, 4, 'Platform  independent'),
(15, 4, 'both a and b'),
(16, 4, 'none of These'),
(17, 5, 'ActionEvent'),
(18, 5, 'keyEvent'),
(19, 5, 'AdjustmentEvent'),
(20, 5, 'windowEvent'),
(21, 6, 'true'),
(22, 6, 'false'),
(23, 7, 'Transmission Control Protocol'),
(24, 7, 'Transfer Control Protocol'),
(25, 7, 'Transmission Control Port'),
(26, 7, 'Transfer communication Protocol'),
(27, 8, 'User Datagram Port'),
(28, 8, 'User Datagram Protocol'),
(29, 8, 'Universal Datagram Protocol'),
(30, 8, 'User Data Protocol'),
(31, 9, 'Java Database Communication'),
(32, 9, 'Java Datagram Connectivity'),
(33, 9, 'Java Database Connectivity'),
(34, 9, 'Java Database Connection'),
(35, 10, 'JDBC drivers,\r\nODBC drivers'),
(36, 10, 'Drivers,\r\nApplication'),
(37, 10, 'ODBC drivers,\r\nJDBC drivers'),
(38, 10, 'Application,\r\ndrivers'),
(39, 11, '<% %>'),
(40, 11, '<%=%>'),
(41, 11, '<@ %= %>'),
(42, 11, 'none of These'),
(43, 12, 'HTML'),
(44, 12, 'JSP'),
(45, 12, 'both a and b'),
(46, 12, 'none Of these'),
(47, 13, 'Platform dependent'),
(48, 13, 'platform independent'),
(49, 13, 'both a and b'),
(50, 13, 'none of these'),
(51, 14, 'Button()'),
(52, 14, 'Button(String text)'),
(53, 14, 'both a and b'),
(54, 14, 'none of these'),
(55, 15, 'Label'),
(56, 15, 'Checkbox'),
(57, 15, 'menu'),
(58, 15, 'none of these'),
(59, 16, 'Component'),
(60, 16, 'Container'),
(61, 16, 'both a and b'),
(62, 16, 'All of the above'),
(63, 17, 'Button'),
(64, 17, 'CheckboxGroup'),
(65, 17, 'TextField'),
(66, 17, 'Label'),
(67, 18, 'Panel'),
(68, 18, 'Window'),
(69, 18, 'ScrollPane'),
(70, 18, 'All of the above'),
(71, 19, 'window'),
(72, 19, 'Panel'),
(73, 19, 'none of these'),
(74, 19, 'all of the above'),
(75, 20, 'frame.Settitle(title);'),
(76, 20, 'frame.setTitle(title);'),
(77, 20, 'frame.setTitle(title)'),
(78, 20, 'frame.setTitle();'),
(79, 21, 'frame.SetSize();'),
(80, 21, 'frame.setSize(x,y)'),
(81, 21, 'frame.setsize(x,y);'),
(82, 21, 'frame.setSize(x,y);'),
(83, 22, 'Yes'),
(84, 22, 'No'),
(85, 23, 'setMenuBar()'),
(86, 23, 'setMenubar()'),
(87, 23, 'SetMenuBar()'),
(88, 23, 'none of the above'),
(89, 24, 'button'),
(90, 24, 'textfield'),
(91, 24, 'title bar and menu bars'),
(92, 24, 'labels'),
(93, 25, 'modeless'),
(94, 25, 'modal'),
(95, 25, 'system modal'),
(96, 25, 'all of the above'),
(97, 26, 'The checkbox are used for getting multiple selection.\r\nThe radio button are used for a single selection'),
(98, 26, 'checkBox are represented by the square box and RadioButton are represented by circle.'),
(99, 26, 'both a and b'),
(100, 26, 'none of these'),
(101, 27, 'list'),
(102, 27, 'choice'),
(103, 27, 'radioButton'),
(104, 27, 'none of the above'),
(105, 28, 'AWT is heavyweight while Swing components are lightweight'),
(106, 28, 'AWT is Lightweight while Swing components are heavyweight'),
(107, 28, 'both a and b'),
(108, 28, 'none of these'),
(109, 29, 'JComboBox'),
(110, 29, 'JButton'),
(111, 29, 'JCheckBox'),
(112, 29, 'All of the above'),
(113, 30, 'JComboBox()'),
(114, 30, 'JComboBox(ComboBoxModel M)'),
(115, 30, 'both a and b'),
(116, 30, 'none of these'),
(117, 31, 'JCheckBox()'),
(118, 31, 'JCheckBox(String s)'),
(119, 31, 'JCheckBox(String s,boolean selected) '),
(120, 31, 'all of the above'),
(121, 32, 'AbstractButton'),
(122, 32, 'JMenuItem'),
(123, 32, 'bothe a and b'),
(124, 32, 'none of these'),
(125, 33, 'Applet'),
(126, 33, 'Event'),
(127, 33, 'ComponentEvent'),
(128, 33, 'InputEvent'),
(129, 34, '2'),
(130, 34, '3'),
(131, 34, '4'),
(132, 34, '5'),
(133, 35, 'canvas'),
(134, 35, 'frame'),
(135, 35, 'window'),
(136, 35, 'all of these'),
(137, 36, 'new ImageIcon (\"c:\\image\\us.gif\")'),
(138, 36, 'new ImageIcon (\"c:\\\\image\\\\us.gif\")'),
(139, 36, 'new ImageIcon (\"c:\\image\\US.gif\")'),
(140, 36, 'none of these'),
(141, 37, 'ButtonControl class'),
(142, 37, 'AbstractButton class'),
(143, 37, 'none of these'),
(144, 37, 'all of the above'),
(145, 38, 'FlowLayout'),
(146, 38, 'GridLayout'),
(147, 38, 'BorderLayout'),
(148, 38, 'GridbagLayout'),
(149, 39, 'Labels'),
(150, 39, 'Buttons'),
(151, 39, 'CheckBox'),
(152, 39, 'All of the above'),
(153, 40, 'menu'),
(154, 40, 'applet'),
(155, 40, 'both a and b'),
(156, 40, 'none of these'),
(157, 41, '10'),
(158, 41, '5'),
(159, 41, '0'),
(160, 41, '20'),
(161, 42, 'setRolloverIcon(Icon icon_name)'),
(162, 42, 'setRollover(Icon icon_name)'),
(163, 42, 'SetRolloverIcon(Icon icon_name)'),
(164, 42, 'none of thses'),
(165, 43, 'flowLayout'),
(166, 43, 'GridLayout'),
(167, 43, 'BorderLayout'),
(168, 43, 'cardLayout'),
(169, 44, 'Window'),
(170, 44, 'Panel'),
(171, 44, 'both a and b'),
(172, 44, 'none of these'),
(173, 45, '2'),
(174, 45, '3'),
(175, 45, '4'),
(176, 45, '5'),
(177, 46, 'use a port number that is automatically allocated.'),
(178, 46, 'use a port number that is not allocated.'),
(179, 46, 'none of yhese'),
(180, 46, 'all of the above'),
(181, 47, '1024'),
(182, 47, '1020'),
(183, 47, '120'),
(184, 47, '1111'),
(185, 48, 'getActionEvent()'),
(186, 48, 'getCommand()'),
(187, 48, 'getActionCommand()'),
(188, 48, 'getActionEventCommand()'),
(189, 49, 'int'),
(190, 49, 'void'),
(191, 49, 'boolean'),
(192, 49, 'String'),
(193, 50, 'int'),
(194, 50, 'boolean'),
(195, 50, 'float'),
(196, 50, 'void'),
(197, 51, 'void '),
(198, 51, 'String'),
(199, 51, 'boolean'),
(200, 51, 'float'),
(201, 52, 'Model'),
(202, 52, 'Viewer'),
(203, 52, 'view'),
(204, 52, 'Controller'),
(205, 53, 'int'),
(206, 53, 'String'),
(207, 53, 'container'),
(208, 53, 'void'),
(209, 54, 'setIcon()'),
(210, 54, 'getText()'),
(211, 54, 'setLabel()'),
(212, 54, 'setBordeLayout()'),
(213, 55, '2'),
(214, 55, '3'),
(215, 55, '4'),
(216, 55, '5'),
(217, 56, '1'),
(218, 56, '2'),
(219, 56, '3'),
(220, 56, '4'),
(221, 57, 'Icon class'),
(222, 57, 'Icon Interface'),
(223, 57, 'Image class'),
(224, 57, 'Image Interface'),
(225, 59, 'Applet'),
(226, 59, 'Event'),
(227, 59, 'ComponentEvent'),
(228, 59, 'InputEvent'),
(229, 60, 'new ImageIcon (\"c:\\image\\us.gif\")'),
(230, 60, 'new ImageIcon (\"c:\\\\image\\\\us.gif\")'),
(231, 60, 'new ImageIcon (\"c:\\image\\US.gif\")'),
(232, 60, 'none of these'),
(233, 61, 'title bar, border, resizing corner'),
(234, 61, 'title bar, border'),
(235, 61, 'resizing corner'),
(236, 61, 'title bar, border,resizing corner, color'),
(237, 62, 'Windows, Panel, ScrollPane'),
(238, 62, 'ScrollPane, Vector, String'),
(239, 62, 'Thread, Vector, String '),
(240, 62, 'Thread, Vector'),
(241, 63, 'JComboBox'),
(242, 63, 'JTabbedPane'),
(243, 63, 'JScrollPane'),
(244, 63, 'Jtree'),
(245, 64, 'JTable object displays rows of data.'),
(246, 64, 'JTable object displays columns of data.'),
(247, 64, 'JTable object displays rows and columns of data.'),
(248, 64, 'JTable object displays data in Tree form'),
(249, 65, 'setBounds '),
(250, 65, 'setSize'),
(251, 65, 'both a and b '),
(252, 65, 'none of these'),
(253, 66, 'getPoint()'),
(254, 66, 'translatePoint()'),
(255, 66, 'getClickCount()'),
(256, 66, 'isPopupTrigger()'),
(257, 67, 'KeyPressed()'),
(258, 67, 'KeyReleased()'),
(259, 67, 'KeyTyped()'),
(260, 67, 'KeyTyped()'),
(261, 68, 'getPoint() '),
(262, 68, 'GetCoordinates()'),
(263, 68, 'GetMouseXY() '),
(264, 68, 'GetMouseCoordinates()'),
(265, 69, 'WindowEvent'),
(266, 69, 'ComponentEvent'),
(267, 69, 'ItemEvent'),
(268, 69, 'InputEvent'),
(269, 70, 'WindowEvent '),
(270, 70, 'ComponentEvent'),
(271, 70, 'ItemEvent'),
(272, 70, 'InputEvent'),
(273, 71, 'java.lang'),
(274, 71, 'java.awt'),
(275, 71, 'java.awt.event'),
(276, 71, 'java.event'),
(277, 72, 'MouseDragged()'),
(278, 72, 'MousePressed() '),
(279, 72, 'MouseReleased() '),
(280, 72, 'MouseClicked()'),
(281, 73, 'BLOCK_DECREMENT'),
(282, 73, 'BLOCK_INCREMENT'),
(283, 73, 'UNIT_DECREMENT'),
(284, 73, 'UNIT_INCREMENT'),
(285, 74, 'WINDOW_CLOSED'),
(286, 74, 'WINDOW_CLOSING'),
(287, 74, 'WINDOW_ACTIVATED'),
(288, 74, 'WINDOW_DEACTIVATED'),
(289, 75, 'AdjustmentListener'),
(290, 75, 'MousseListener'),
(291, 75, 'ItemListener'),
(292, 75, 'WindowListener'),
(293, 76, 'ComponentEvent'),
(294, 76, 'ContainerEvent'),
(295, 76, 'FocusEvent'),
(296, 76, 'InputEvent'),
(297, 77, 'COMPONENT_HIDDEN'),
(298, 77, 'COMPONENT_MOVED\r\n'),
(299, 77, 'COMPONENT_RESIZE'),
(300, 77, 'all of these'),
(301, 78, 'AwtEvent'),
(302, 78, 'KeyEvent'),
(303, 78, 'ActionEvent'),
(304, 78, 'AdjustmentEvent'),
(305, 79, 'Container Event'),
(306, 79, 'FocusEvent'),
(307, 79, 'both a and b'),
(308, 79, 'none of these'),
(309, 80, 'java.io'),
(310, 80, 'java.lang'),
(311, 80, 'java.net'),
(312, 80, 'java.util'),
(313, 81, 'change in the state of objec'),
(314, 81, 'change in the state of variable '),
(315, 81, 'bothe a and b'),
(316, 81, 'none of these'),
(317, 82, 'getValue()'),
(318, 82, 'getAdjustmentType()'),
(319, 82, 'getAdjustmentValue()'),
(320, 82, 'getAdjustmentAmount()'),
(321, 83, 'getPoint()'),
(322, 83, 'getCoordinates()'),
(323, 83, 'GetMouseXY()'),
(324, 83, 'getMouseCoordinates()'),
(325, 84, 'ComponetListener'),
(326, 84, 'ContainerListener'),
(327, 84, 'ActionListener'),
(328, 84, 'InputListener'),
(329, 85, 'ActionEvent'),
(330, 85, 'ComponentEvent'),
(331, 85, 'AdjustmentEvent'),
(332, 85, 'WindowEvent'),
(333, 86, 'getType()'),
(334, 86, 'getEventType() '),
(335, 86, 'getAdjustmentType()'),
(336, 86, 'getEventObjectType()'),
(337, 87, 'getcomponent()'),
(338, 87, 'getChild()'),
(339, 87, 'getcontainercomponent()'),
(340, 87, 'getcomponentChild()'),
(341, 88, 'addMouse()'),
(342, 88, 'addMouseListener()'),
(343, 88, 'addMouseMotionListener()'),
(344, 88, 'eventMouseMotionListener()'),
(345, 89, 'An event is an object that describe a state change in a source.'),
(346, 89, 'An event is an object that describe a state change in a processing.'),
(347, 89, 'An event is an object that describe a any changes by the user and system'),
(348, 89, 'An event is a class used for defining\r\nobject, to create events.'),
(349, 90, 'ComponentListener'),
(350, 90, 'AdjustmentListener'),
(351, 90, 'FocusListener'),
(352, 90, 'InputListener'),
(353, 91, 'ComponentListener'),
(354, 91, 'ContainerListener'),
(355, 91, 'FocusListener'),
(356, 91, 'InputListener'),
(357, 92, 'getcomponent()'),
(358, 92, 'getChild()'),
(359, 92, 'getcontainercomponent()'),
(360, 92, 'getcomponentChild()'),
(361, 93, 'add or remove'),
(362, 93, 'gain or loses'),
(363, 93, 'both a and b'),
(364, 93, 'none of these'),
(365, 94, 'addActionListener(p)'),
(366, 94, 'jbt.addActionListener(p)'),
(367, 94, 'jbt.addEventActionListener(p) '),
(368, 94, 'jbt.EventListener(p)'),
(369, 96, 'mouseDragged()'),
(370, 96, 'mouseEntered()'),
(371, 96, 'mousePressed()'),
(372, 96, 'all '),
(373, 95, 'windowOpening'),
(374, 95, 'WindowActivated'),
(375, 95, 'WindowIconified '),
(376, 95, 'WindowClosed'),
(377, 97, 'true'),
(378, 97, 'flase'),
(379, 98, '10'),
(380, 98, '1024'),
(381, 98, '2048'),
(382, 98, '512'),
(383, 99, 'java.io'),
(384, 99, 'java.util'),
(385, 99, 'java.net'),
(386, 99, 'java.network'),
(387, 100, 'DatagramPacket'),
(388, 100, 'URL'),
(389, 100, 'InetAddress'),
(390, 100, 'ContentHandler'),
(391, 101, 'Port number'),
(392, 101, 'Host name'),
(393, 101, 'File Path'),
(394, 101, 'Protocol'),
(395, 102, '80 '),
(396, 102, '53 '),
(397, 102, '110 '),
(398, 102, '119'),
(399, 103, 'True'),
(400, 103, 'Flase'),
(401, 104, 'HTTP  '),
(402, 104, 'IP'),
(403, 104, 'UDP'),
(404, 104, 'All of above'),
(405, 105, 'getLocalHost() '),
(406, 105, 'getByName() '),
(407, 105, 'getAllByName()'),
(408, 105, 'none of these'),
(409, 106, 'getLocalHost() '),
(410, 106, 'getByName() '),
(411, 106, 'getAllByName()'),
(412, 106, 'None of these'),
(413, 107, 'Host Name '),
(414, 107, 'Port Number '),
(415, 107, 'File Path '),
(416, 107, 'Protocol'),
(417, 108, 'URL(String protocolName,String hostName, intport, String path)'),
(418, 108, 'URL(String urlSpecifier)'),
(419, 108, 'URL(String protocolName,String hostName,String path)'),
(420, 108, 'All of above'),
(421, 109, 'URLNotFound'),
(422, 109, 'URLSourceNotFound'),
(423, 109, ' MalformedURLException'),
(424, 109, 'URLNotFoundException'),
(425, 110, 'URL'),
(426, 110, 'URL Connection '),
(427, 110, 'DatagramSocket '),
(428, 110, 'None of these'),
(429, 111, '8 '),
(430, 111, '16 '),
(431, 111, '32'),
(432, 111, '64'),
(433, 112, 'Datagram '),
(434, 112, 'server '),
(435, 112, 'clientsocket '),
(436, 112, 'None of these'),
(437, 113, 'UnknownException'),
(438, 113, 'MalformEsception '),
(439, 113, 'UnknownHostException'),
(440, 113, 'none of these'),
(441, 114, 'static\r\nInetAddressgetLocalHost( )throws UnknownHostException'),
(442, 114, 'static\r\nInetAddressgetByName(String\r\nhostName)throws UnknownHostException'),
(443, 114, 'static InetAddress[]getAllByName\r\n(String hostname throws UnknownHostException'),
(444, 114, ' string getHostAddress()'),
(445, 115, 'TCP/IP'),
(446, 115, 'DNS'),
(447, 115, 'Socket '),
(448, 115, 'Proxy Server'),
(449, 116, 'java.net.BindExceptionError occur'),
(450, 116, 'the server is created with no problem'),
(451, 116, 'the server is blocked until the port is available'),
(452, 116, 'the server\r\nencounteres a fatal error and must be\r\nterminated.'),
(453, 117, 'getData()'),
(454, 117, 'getBytes()'),
(455, 117, 'getArray()'),
(456, 117, 'receiveBytes()'),
(457, 118, '25'),
(458, 118, '75'),
(459, 118, '50'),
(460, 118, '15'),
(461, 119, 'accept() '),
(462, 119, 'wait()'),
(463, 119, 'resume()'),
(464, 119, 'none of these'),
(465, 120, 'DatagramPacket'),
(466, 120, 'DatagramSocket'),
(467, 120, 'Both'),
(468, 120, 'none of these'),
(469, 121, 'ContentType()'),
(470, 121, 'contentType()'),
(471, 121, 'getContentType()'),
(472, 121, 'GetContentType()'),
(473, 122, 'DatagramSocket(int port)'),
(474, 122, 'DatagramSocket(int port, InetAddress address)'),
(475, 122, 'DatagramSocket()'),
(476, 122, 'none of these'),
(477, 123, 'true'),
(478, 123, 'false'),
(479, 124, 'ResultSet executeQuery()'),
(480, 124, 'execute()'),
(481, 124, 'execute update()'),
(482, 124, 'none of these'),
(483, 125, 'JDBC drivers, ODBC drivers'),
(484, 125, 'Drivers, Application'),
(485, 125, 'ODBC drivers, JDBC drivers'),
(486, 125, 'Application, drivers'),
(487, 126, 'JDBC calls, network protocol\r\n'),
(488, 126, 'ODBC class, network protocol'),
(489, 126, 'ODBC class, user call'),
(490, 126, 'JDBC class, user call'),
(491, 127, 'executable'),
(492, 127, 'simple'),
(493, 127, 'high level'),
(494, 127, 'parameterized '),
(495, 128, '1'),
(496, 128, '2'),
(497, 128, '3'),
(498, 128, '4'),
(499, 129, 'Both type 1 and 2 '),
(500, 129, 'Both type 1 and 3\r\n'),
(501, 129, 'Both type 3 and 4'),
(502, 129, 'only type 4'),
(503, 130, 'afterLast() '),
(504, 130, 'beforeFirst() '),
(505, 130, 'next() '),
(506, 130, 'All of the above'),
(507, 131, 'PreparedStatement class'),
(508, 131, 'collableStatement class'),
(509, 131, 'both a and b'),
(510, 131, 'none of these'),
(511, 132, 'Forward only '),
(512, 132, 'backward only'),
(513, 132, 'both'),
(514, 132, 'none of these'),
(515, 133, 'JDBC-ODBC bridge'),
(516, 133, 'JDBC-Native API'),
(517, 133, 'JDBC-Net pure Java'),
(518, 133, '100% Pure Java'),
(519, 134, 'select * from\r\ninfromation_schema,table\r\n'),
(520, 134, 'select TABLE_NAME from INFORMATION_S\r\nCHEMA.TABLES where TABLE_TYPE =\'BASE TABLE\''),
(521, 134, 'both'),
(522, 134, 'none of these'),
(523, 135, 'executeQuery()'),
(524, 135, 'executeUpdate()'),
(525, 135, 'getConnection() '),
(526, 135, 'preparecall()'),
(527, 136, 'getResultSet '),
(528, 136, 'getColumnName'),
(529, 136, 'getMetaData'),
(530, 136, 'getData'),
(531, 137, 'beforeFirst'),
(532, 137, 'afterLast'),
(533, 137, 'previous'),
(534, 137, 'all\r\n'),
(535, 138, 'JDBC drivers, ODBC drivers'),
(536, 138, 'Drivers, Application'),
(537, 138, 'ODBC drivers, JDBC drivers'),
(538, 138, 'Application, drivers'),
(539, 139, 'Single row'),
(540, 139, 'ResultSet object'),
(541, 139, 'Single Column'),
(542, 139, 'Database Table'),
(543, 140, 'Standard query language'),
(544, 140, 'Sequential query language'),
(545, 140, 'Structured query language'),
(546, 140, 'Server side query language'),
(547, 141, 'one'),
(548, 141, 'two'),
(549, 141, 'three'),
(550, 141, 'four'),
(551, 142, 'Type 1'),
(552, 142, 'Type 2'),
(553, 142, 'Type 3'),
(554, 142, 'Type 4'),
(555, 143, 'executeUpdate()'),
(556, 143, 'executeDeleteQuery()'),
(557, 143, 'execute()'),
(558, 143, 'nonexecute()'),
(559, 144, 'A JDBC RowSet object holds tabular\r\ndata in a way that makes it more\r\nflexible and easier to use than a result set.'),
(560, 144, 'A RowSet objectsare JavaBeans component.'),
(561, 144, 'Both of the above'),
(562, 144, 'none of these'),
(563, 145, 'Class.loadClass(\"sun.jdbc.odbc.JdbcO dbcDriver\") '),
(564, 145, 'Class.loadClass(sun.jdbc.odbc.JdbcO dbcDriver) '),
(565, 145, 'Class.forName(\"sun.jdbc.odbc.Jdbc\r\nOdbcDriver\")'),
(566, 145, 'Class.forName(sun.jdbc.odbc.Jdbc\r\nOdbcDriver)'),
(567, 146, 'redirectURL()'),
(568, 146, 'sendRedirct()'),
(569, 146, 'redirctHttp()'),
(570, 146, 'getRequestDispatcher()'),
(571, 147, 'a,b,c are compulsary'),
(572, 147, 'only a and c'),
(573, 147, 'only a'),
(574, 147, 'only a and b'),
(575, 148, 'interface'),
(576, 148, 'Platform independent'),
(577, 148, 'both a and b'),
(578, 148, 'none of these'),
(579, 149, 'javax.servlet.cookie'),
(580, 149, 'javax.servlet.http.cookie'),
(581, 149, 'javax.servlet.cookie'),
(582, 149, 'javax.Servlet.http.cookie'),
(583, 150, 'javax.servlet.Servlet\r\ninterface'),
(584, 150, 'javax.servlet.servletConfig interface'),
(585, 150, 'both a and b'),
(586, 150, 'none of these'),
(587, 151, 'parameter passes to the servlet'),
(588, 151, 'enumeration of all the initialization\r\nparameter names.'),
(589, 151, 'an object of ServletContext.'),
(590, 151, 'the name of the servlet'),
(591, 152, 'cleanup'),
(592, 152, 'Service()'),
(593, 152, 'destroy() '),
(594, 152, 'none of these'),
(595, 153, 'CGI is not platform independent but\r\nservlet are platform independent'),
(596, 153, 'CGI is platform independent but\r\nservlet are platform depended'),
(597, 154, '2kb '),
(598, 154, '3kb '),
(599, 154, '4kb '),
(600, 154, '5kb '),
(601, 155, 'PUT Method'),
(602, 155, 'FTP Method'),
(603, 155, 'WRITE Method'),
(604, 155, 'COPY Method'),
(605, 156, 'Servlet, HTML'),
(606, 156, 'HTML, Java'),
(607, 156, 'HTML, Servlet'),
(608, 156, ' Java, HTML'),
(609, 157, '2'),
(610, 157, '3'),
(611, 157, '4'),
(612, 157, '5'),
(613, 158, 'JspInit()'),
(614, 158, 'jspService()'),
(615, 158, 'jspDestroy()'),
(616, 158, 'none of these'),
(617, 159, '1 and 2'),
(618, 159, '1 and 3'),
(619, 159, 'only 3'),
(620, 159, 'all of the above'),
(621, 160, 'VBScript Only'),
(622, 160, 'Jscript only'),
(623, 160, 'Java only'),
(624, 160, 'All'),
(625, 161, 'request.getParameter()'),
(626, 161, 'request.setParameter()'),
(627, 161, 'responce.getParameter()'),
(628, 161, 'responce.getAttribute()'),
(629, 162, 'web component response settings'),
(630, 162, 'web component settings'),
(631, 162, 'web component request objects'),
(632, 162, 'All'),
(633, 163, 'GET method'),
(634, 163, 'POST method'),
(635, 163, 'HEAD method'),
(636, 163, 'PUT method'),
(637, 164, 'A session refers to the entire interaction between client and server from the time of client logon to logout\r\nfrom system'),
(638, 164, 'A session is a time taken by server'),
(639, 164, 'A session refers to time taken by client'),
(640, 164, 'A session is a interaction\r\n'),
(641, 165, 'Open Notepad, Write HTML\r\ncode, Save file with extension\r\n.html, go to explorer and write\r\ncommand\r\nhttp://location:8080/created\r\nfolder name/*.html'),
(642, 165, 'Write HTML code, Save file\r\nwith extension .html, go to\r\nexplorer and write command\r\nhttp://location:8080/created\r\nfolder name/*.html, open\r\nnotepad'),
(643, 165, 'Open Notepad, Save file with\r\nextension .html, go to explorer\r\nand write command\r\nhttp://location:8080/created\r\nfolder name/*.html, Write HTML\r\ncode'),
(644, 165, 'go to explorer and write\r\ncommand\r\nhttp://location:8080/created\r\nfolder name/*.html, Open\r\nNotepad, Write HTML code,\r\nSave file with extension .html'),
(645, 166, 'Browser will interpret content as source code.'),
(646, 166, 'Browser will take contents as plain text'),
(647, 166, 'Browser will interpret content as HTML source code'),
(648, 166, 'Browser will interpret content as Java source code'),
(649, 167, 'void service(ServletRequest req, ServletResponse res)'),
(650, 167, 'void service(ServletResponse res ServletRequest req, )\r\n'),
(651, 167, 'void service(ServletRequest req, ServletRequest req )'),
(652, 167, 'void service(Servlet Response req, ServletResponse res)'),
(653, 168, 'Non-Persistent Cookies'),
(654, 168, 'Persistent Cookies'),
(655, 168, 'both a and b'),
(656, 168, 'none of these'),
(657, 169, 'destroy()'),
(658, 169, 'getServletConfig()'),
(659, 169, 'init()'),
(660, 169, 'none of these '),
(661, 170, 'state information of user like name,\r\naddress'),
(662, 170, 'Client Information'),
(663, 170, 'Server Information'),
(664, 170, 'Program Information'),
(665, 58, 'ImageIcon(String filename)'),
(666, 58, 'ImageIcon(URL url)'),
(667, 58, 'both'),
(668, 58, 'none of thses');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_number` tinyint(3) NOT NULL,
  `marks` tinyint(3) NOT NULL,
  `type` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `subject_id`, `chapter_number`, `marks`, `type`) VALUES
(1, 'AWT stands for?', 1, 1, 1, 1),
(2, 'How many Constructors are in Label', 1, 1, 1, 1),
(3, 'MVC stands for', 1, 2, 1, 1),
(4, 'Swing is ', 1, 2, 1, 1),
(5, 'When we click on a button, which event is generated', 1, 3, 1, 1),
(6, 'There is no adapter class for ActionListener. True or false? ', 1, 3, 1, 1),
(7, 'TCP stands for', 1, 4, 1, 1),
(8, 'UDP stands for', 1, 4, 1, 1),
(9, 'JDBC stands for', 1, 5, 1, 1),
(10, 'The JDBC-ODBC bridge allows ……….. to be\r\nused as ………..', 1, 5, 1, 1),
(11, 'JSP starts and end with symbol', 1, 6, 1, 1),
(12, 'A JSP page consists of which tags?', 1, 6, 1, 1),
(13, 'AWT is', 1, 1, 1, 1),
(14, 'The constructors of Button class is/are?', 1, 1, 1, 1),
(15, 'Which of the following component class cannot be add on applet', 1, 1, 1, 1),
(16, 'A ScrollPane is _______________', 1, 1, 1, 1),
(17, 'A ____________ is a passive AWT control which do not generate any event.', 1, 1, 1, 1),
(18, 'Who is the parent class of Frame?', 1, 1, 1, 1),
(19, 'Who is the parent class of Applet?', 1, 1, 1, 1),
(20, 'Which method is used to set title of a frame?', 1, 1, 1, 1),
(21, 'Which method is used to set size of a frame?', 1, 1, 1, 1),
(22, 'Can we add panel on a frame?', 1, 1, 1, 1),
(23, 'Which method is used to set menu-bar of a frame', 1, 1, 1, 1),
(24, 'What does a frame have that panel doesn\'t?', 1, 1, 1, 1),
(25, 'Types of DialogBox are', 1, 1, 1, 1),
(26, 'What is the difference between Checkbox and RadioButton?', 1, 1, 1, 1),
(27, 'CheckboxGroup is used to create', 1, 1, 1, 1),
(28, 'What is the difference between Swing and AWT?', 1, 2, 2, 1),
(29, 'Which of the below class belongs to Swing API?', 1, 2, 2, 1),
(30, 'Which of the below are the constructor of JComboBox?', 1, 2, 2, 1),
(31, 'Select a appropriate constructor for JCheckBox?', 1, 2, 1, 1),
(32, 'JButton is sub-class of', 1, 2, 1, 1),
(33, 'Which of these is a super class of all adapter class?', 1, 2, 1, 1),
(34, 'How many constructors are of JTextArea?', 1, 2, 1, 1),
(35, 'Which of the following is not a swing class', 1, 2, 1, 1),
(36, 'The method _________create a ImageIcon from file c:\\image\\us.gif', 1, 2, 1, 1),
(37, 'RadioButton is sub-class of', 1, 2, 1, 1),
(38, 'JPanel and Applet use ___________________ as their default layout', 1, 1, 1, 1),
(39, 'Which are various AWT controls from following?', 1, 1, 1, 1),
(40, 'Which of the following component class cannot be add on container', 1, 1, 1, 1),
(41, 'By default page-up and page-down increment of scrollbar is_____.\r\n', 1, 1, 1, 1),
(42, 'Which method is used to display icon on a Button?', 1, 1, 2, 1),
(43, 'By default which layout manager is set on applet', 1, 1, 1, 1),
(44, 'Canvas is a ________________ ', 1, 1, 1, 1),
(45, 'Border Layout is divided into_________regions', 1, 1, 1, 1),
(46, 'Calling ServerSocket() constructor with port value \'zero\' means______', 1, 3, 1, 1),
(47, 'TCP/IP reserves the ______ ports for specific protocols ', 1, 3, 1, 1),
(48, 'How to obtain the command name for invoking\r\nActionEvent?', 1, 2, 1, 1),
(49, 'What is return type of actionPerformed(ActionEvent e)', 1, 2, 1, 1),
(50, 'Return type of itemStateChanged(ItemEvent ie) ', 1, 2, 1, 1),
(51, 'Return type of removeActionListener(ActionListener el)', 1, 2, 1, 1),
(52, 'MVC architeccture has 3 major parts, which is the bottom most\r\nlevel of the MVC architecture', 1, 2, 1, 1),
(53, 'getContentPane() has return type?', 1, 2, 1, 1),
(54, 'Which of these methods cannot be called on JLabel object?\r\n', 1, 2, 1, 1),
(55, 'How many constructors of JButton present?', 1, 2, 1, 1),
(56, 'How many constructors of JLabel present?', 1, 2, 1, 1),
(57, 'ImageIcon class implements?', 1, 2, 1, 1),
(58, 'Which of the following are constructor of ImageIcon?', 1, 2, 1, 1),
(59, 'Which of these is a super class of all adapter class?', 1, 2, 2, 1),
(60, 'The method _________create a ImageIcon from file c:\\image\\us.gif', 1, 2, 2, 1),
(61, 'By default the Frame has a __________', 1, 2, 2, 1),
(62, 'What are the subclasses of the Container class?', 1, 2, 2, 1),
(63, '____ is a combination of textfield and drop-down list that lets user either type in a value or select it from a list that is displayed when the user asks for it', 1, 2, 2, 1),
(64, 'What is the purpose of JTable?', 1, 2, 1, 1),
(65, 'When layout manager is disabled, which methods is used to determine the shape and position of component?', 1, 2, 2, 1),
(66, 'Method use to change the location of event?', 1, 3, 1, 1),
(67, 'Which method will be invoked if a character is entered?', 1, 3, 1, 1),
(68, 'Which method used to obtain co-ordinates of a mouse', 1, 3, 1, 1),
(69, 'Which of these is a super class of all ContainerEvent class?', 1, 3, 1, 1),
(70, '____is a superclass of windowEvent class', 1, 3, 1, 1),
(71, 'Which of these package contains all the event handling interfaces?', 1, 3, 1, 1),
(72, '________method is defined in mousemotionAdapter class', 1, 3, 1, 1),
(73, 'Which of these constant value will change when the button at the end of\r\nscrollbar was clicked to increase its vvalue?', 1, 3, 1, 1),
(74, 'Which constant of window Event class makes a request for closing window', 1, 3, 1, 1),
(75, 'ScrollBar implements____ ', 1, 3, 1, 1),
(76, 'Which of these events is generated when the size of component is changed?', 1, 3, 1, 1),
(77, 'Which of these are integer constants of ComponentEvent class?', 1, 3, 1, 1),
(78, 'Base class of all AWT Event class is', 1, 3, 1, 1),
(79, 'ComponentEvent is a super class of _____', 1, 3, 1, 1),
(80, 'Event class is defined in which of these libraries', 1, 3, 1, 1),
(81, 'Event is called as', 1, 3, 1, 1),
(82, 'Which of these method can be used to know the degree of adjustment made by the user?', 1, 3, 1, 1),
(83, 'Which of these methods can be used to obtain the coordinates of a mouse?', 1, 3, 1, 1),
(84, 'Which of these interfaces defines method actionPerformed()?', 1, 3, 1, 1),
(85, 'If scrollbar is manipulated ____ event will be notified.', 1, 3, 1, 1),
(86, '____method can be used to determine the type of adjustment event.', 1, 3, 1, 1),
(87, 'which of the following method can be used to get reference to a component that was affected by the container?', 1, 3, 1, 1),
(88, 'Which of these methods are used to register a mouse motion listener? ', 1, 3, 1, 1),
(89, 'What is an event delegation model used by Java programming languages?', 1, 3, 1, 1),
(90, 'For which Interface method belongs to void\r\nadjustmentValueChanged(AdjustmentEvent e)', 1, 3, 1, 1),
(91, 'Which of these interfaces defines four methods?', 1, 3, 1, 1),
(92, 'Which of these methods can be used to get reference to a component that was removed from a container?', 1, 3, 2, 1),
(93, 'FocusEvent are fired whenever component _____ the focus ', 1, 3, 2, 1),
(94, 'Which of the following statements registers a panel object p as a listener for a button variable jbt?', 1, 3, 2, 1),
(95, 'Which of the following is not one of the seven methods for handling\r\nwindow events?', 1, 3, 2, 1),
(96, 'Which of these method will respond when you click any button by mouse', 1, 3, 2, 1),
(97, 'Adapter classes are similar to EventListener interfaces', 1, 3, 1, 1),
(98, 'How many ports of TCP/IP are reserved for specific protocols?', 1, 4, 1, 1),
(99, 'Which of these package contains classes and interfaces for networking?', 1, 4, 1, 1),
(100, 'Which of these class is used to encapsulate IP address and DNS?', 1, 4, 1, 1),
(101, 'In the format for defining URL what is the last part?', 1, 4, 1, 1),
(102, 'Port number of HTTP is ___ ', 1, 4, 1, 1),
(103, 'InetAddress class has no visible constructors ', 1, 4, 1, 1),
(104, 'Which of the following connection oriented protocol?', 1, 4, 1, 1),
(105, 'The factory method which returns an array of InetAddress that represent\r\nall of the addresses that particular host name resolves to. ', 1, 4, 1, 1),
(106, 'The ___ method simply returns the InetAddress object that repersents the local host.', 1, 4, 1, 1),
(107, 'What is the first part of URL address?', 1, 4, 1, 1),
(108, 'select the proper constructor of URL class', 1, 4, 1, 1),
(109, 'Which of these exception is thrown by URL class’s constructors?', 1, 4, 1, 1),
(110, 'To access attributes of remote host which of the following class is used', 1, 4, 2, 1),
(111, 'How many bits are in a single IP address?', 1, 4, 1, 1),
(112, 'byte[] getData() method is defined by ____ class ', 1, 4, 1, 1),
(113, 'Which exeception is thrown by InetAddress class when it cant resolve the name of address?', 1, 4, 2, 1),
(114, 'Select the proper method for retriving the host name of local machine', 1, 4, 2, 1),
(115, 'Which of these are protocol for breaking and sending packets to an address across a network? ', 1, 4, 2, 1),
(116, 'When creating a server on port that is already in use, _____', 1, 4, 1, 1),
(117, 'Which of these methods of DatagramPacket issued to obtain the byte array of data contained in a datagram?', 1, 4, 1, 1),
(118, 'what is the default length of the queue in following constructor of Serversocket?ServerSocket(int portno)', 1, 4, 1, 1),
(119, 'which method of ServerSocket will wait for a client to initiate\r\ncommunications and then communicate with the client', 1, 4, 1, 1),
(120, 'Which of these class is necessary to implement datagrams?', 1, 4, 1, 1),
(121, 'Which of these methods is used to know the type of content used in the URL? ', 1, 4, 1, 1),
(122, 'Which constructor of DatagramSocket class is used to creates a datagram socket and binds it with the given Port Number?', 1, 4, 1, 1),
(123, 'The DatagramSocket and DatagramPacket classes are not used for connectionless socket programming.', 1, 4, 2, 1),
(124, 'Which of the following is used generally for reading the content of the database?', 1, 5, 1, 1),
(125, 'The JDBC-ODBC bridge allows ……….. to be\r\nused as ………..', 1, 5, 1, 1),
(126, 'Native API converts ____________ into the ___________ used by DBMS.', 1, 5, 2, 1),
(127, 'Prepared statement object in JDBC used to execute ____ queries.', 1, 5, 1, 1),
(128, 'Prepared Statement updatreemp=con.preparedStatement(\"insert into emp values(?,?,?)\"); How many values are need to iunsert for PreparedStatement parameter?', 1, 5, 1, 1),
(129, 'Which JDBC driver Type(s) can be used in either applet or servlet code?', 1, 5, 1, 1),
(130, 'ResultSet navigation method consist of', 1, 5, 1, 1),
(131, 'prepareStatement method is from which class', 1, 5, 1, 1),
(132, 'We can use ResultSet navigation methods when we have ResultSet that is of type', 1, 5, 1, 1),
(133, 'Which type of driver is unique in JDBC?', 1, 5, 1, 1),
(134, 'What is the query used to display all tables names in SQL Server?', 1, 5, 1, 1),
(135, 'Which statement is static and synchronized in JDBC API', 1, 5, 1, 1),
(136, '____ returns a ResultSetMetaData object describing the ResultSet.', 1, 5, 1, 1),
(137, 'ResultSet navigation method consist of', 1, 5, 1, 1),
(138, 'The JDBC-ODBC bridge allows ____ to be used as ____', 1, 5, 2, 1),
(139, 'executeQuery() method returns__________', 1, 5, 1, 1),
(140, '____ is a full form of SQL', 1, 5, 1, 1),
(141, 'How many JDBC driver types does sun define?', 1, 5, 1, 1),
(142, 'Which JDBC driver Type(s) is (are) the JDBC-ODBC bridge?', 1, 5, 1, 1),
(143, 'The..............method executes a simple query and returns a single Result Set object', 1, 5, 1, 1),
(144, 'Which of the following is correct\r\nabout RowSet?', 1, 5, 2, 1),
(145, 'Which of the following statements loads the JDBC-ODBC driver?', 1, 5, 2, 1),
(146, 'Which method of HTTPServeltResponse is used to redirect an HTTP request to another URL ', 1, 6, 1, 1),
(147, 'In order to create a HTTPServelet following import statement are used:- \r\na.import java.io.*; \r\nb.import javax.servlet.* \r\nc.import javax.servlet.http.*;', 1, 6, 1, 1),
(148, 'Servlet is __________', 1, 6, 1, 1),
(149, 'In servlet programming a cookies is respresented by the cookie class in ____ package', 1, 6, 1, 1),
(150, 'init(), service(), destroy() method are declared in ____', 1, 6, 1, 1),
(151, 'The getInitParameter() method of ServletConfig interface returns_____', 1, 6, 1, 1),
(152, '_____ is destructive phase of JSP life cycle.', 1, 6, 1, 1),
(153, 'Servlet offer following advantages over CGI', 1, 6, 2, 1),
(154, 'what is the limit of the data to be passed from HTML when doGet() is used?', 1, 6, 1, 1),
(155, 'Name the http method used to send resources to the server', 1, 6, 1, 1),
(156, 'JSP embeds in ____ in ____ ', 1, 6, 2, 1),
(157, 'Servlet life cycle have ____ state', 1, 6, 1, 1),
(158, 'JSP page performs request processing by calling ___ method.', 1, 6, 1, 1),
(159, 'Disadvantage of CGI: \r\n1.It has lack of scability and reduced speed.   \r\n2.High performance time \r\n3.CGI is platform dependent', 1, 6, 2, 1),
(160, 'What programming language(s) or scripting language(s) does Java Server Pages (JSP) support?', 1, 6, 1, 1),
(161, 'The doGet() method in the example extracts values of the parameter’s type and number by using __________', 1, 6, 1, 1),
(162, 'A deployment descriptor describes', 1, 6, 1, 1),
(163, 'A user types the URL http://www.msbte.com/result.php. Which HTTP request gets generated. Select the one correct answer', 1, 6, 1, 1),
(164, 'What is session in Servlet programming? ', 1, 6, 2, 1),
(165, 'Find correct Steps for creating and executing HTML file for Servlet programming', 1, 6, 2, 1),
(166, 'What is meaning of following statement\r\nRes.setContentType(“text/html”);', 1, 6, 2, 1),
(167, 'Identify correct syntax of service() method of servlet class', 1, 6, 2, 1),
(168, 'Which cookies it is valid for single Session only, it is removed each time when user closes the browser?\r\n', 1, 6, 1, 1),
(169, 'Which is not method of GenericServlet class', 1, 6, 1, 1),
(170, 'A cookie contains___\r\n', 1, 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `enrollment_number` int(11) NOT NULL,
  `seat_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `enrollment_number`, `seat_number`) VALUES
(1, 'Shraddha Thaker', 1700040197, 103013),
(2, 'Vidhi Rughwani', 1700040198, 103014),
(3, 'Mansi Pandey', 1700040180, 103001),
(4, 'Sarwasvi Ingoley', 1700040185, 103005),
(5, 'Dhanvi Sheth', 1700040190, 103001);

-- --------------------------------------------------------

--
-- Table structure for table `student_test_data`
--

CREATE TABLE `student_test_data` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `alloted_question_id` int(11) NOT NULL,
  `selected_option_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_test_data`
--

INSERT INTO `student_test_data` (`id`, `test_id`, `student_id`, `alloted_question_id`, `selected_option_id`) VALUES
(153, 1, 1, 39, 152),
(154, 1, 1, 23, 85),
(155, 1, 1, 61, 233),
(156, 1, 1, 94, 366),
(157, 1, 2, 27, 103),
(158, 1, 2, 39, 152),
(159, 1, 2, 52, 204),
(160, 1, 2, 34, 130),
(161, 1, 2, 73, 284),
(162, 1, 2, 66, 254);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `subject_code`) VALUES
(1, 'Advance Java', 22516),
(2, 'Environmental studies', 22501),
(3, 'Basic Science', 22018),
(4, 'Emerging Trends', 22527),
(5, 'Management Studies', 22523);

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `conduct_on` date NOT NULL,
  `duration` varchar(255) NOT NULL,
  `marks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`, `conduct_on`, `duration`, `marks`) VALUES
(1, 'Advance Java', '2020-03-02', '00:30:00', 6),
(8, 'EVS', '2020-03-02', '00:15:00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `test_details`
--

CREATE TABLE `test_details` (
  `test_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_number` int(11) NOT NULL,
  `marks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_details`
--

INSERT INTO `test_details` (`test_id`, `subject_id`, `chapter_number`, `marks`) VALUES
(1, 1, 1, 2),
(1, 1, 2, 2),
(1, 1, 3, 2),
(8, 2, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `usertype` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `usertype`, `password`) VALUES
(1, 'student', '12345678'),
(2, 'admin', '12345678');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `correct_answer`
--
ALTER TABLE `correct_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks_obtained`
--
ALTER TABLE `marks_obtained`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_test_data`
--
ALTER TABLE `student_test_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `correct_answer`
--
ALTER TABLE `correct_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `marks_obtained`
--
ALTER TABLE `marks_obtained`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=669;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student_test_data`
--
ALTER TABLE `student_test_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
